# OpenML dataset: 2019-Ironman-World-Championship-Results

https://www.openml.org/d/43482

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Data set containing results from 2019 Ironman World Championship in Kona, Hawaii to help answer the question of which countries produce the fastest overall finish times, as well as fastest swim, bike, and run times.
Content
The data set contains the name of each male and female competitor, the country they are representing, their category (Professional or Age Group), their overall placing, finish time, and swim / T1 / bike / T2 / run splits.
Acknowledgements
Coach Cox (www.coachcox.co.uk)
Inspiration

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43482) of an [OpenML dataset](https://www.openml.org/d/43482). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43482/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43482/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43482/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

